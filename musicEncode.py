import getopt
import multiprocessing.pool
import os
import shlex
import subprocess
import sys

# http://sebastianraschka.com/Articles/2014_multiprocessing_intro.html

__author__ = 'profs89'

encode_commands = []
decode_commands = []
verbose = None


def execute_command(command):

    args = shlex.split(command)
    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=os.environ.copy())
    # out = process.communicate()[0]
    process.communicate()

    if verbose:
        print ">> " + command
        # print out + "\n"


def get_os_param(param):
    if param == "slash":
        if os.name == 'nt':
            return "\\"
        else:
            return "/"

    if param == "space_replace":
        if os.name == 'nt':
            return " "
        else:
            return "\ "

    if param == "quote":
        if os.name == 'nt':
            return "\""
        else:
            return ""


def print_params(folder, acceptable_formats):
    print "==== Params ===="
    print "folder = " + folder
    print "acceptable formats = [" + ";".join(acceptable_formats) + "]"
    print "================"


def run(argv):
    opts, argv = getopt.getopt(argv, "f:v",
                               ["--folder", "--verbose"])
    global verbose
    global encode_commands
    global decode_commands
    folder = "."
    acceptable_formats = ["mp3", "MP3", "wav", "WAV"]

    # Parse arguments
    for opt, arg in opts:

        if opt in ('-f', '--folder'):
            folder = arg

        if opt in ('-v', '--verbose'):
            verbose = True

    print_params(folder, acceptable_formats)

    slash = get_os_param("slash")

    if folder[-1] == slash:
        folder = folder[:-1]

    encode_music(folder, folder, acceptable_formats, slash, False)

    encode_music(folder + slash + "decoded", folder + slash + "decoded", acceptable_formats, slash, True)


def encode_music(folder, folder_base, acceptable_formats, slash, encode):

    print "Reading directory " + folder

    if encode:
        container_dir = "encoded"
        action_text = "Encoding"
        command = "lame --cbr -b 320 -q 0 --replaygain-accurate"
        print "\n Encode"
    else:
        container_dir = "decoded"
        action_text = "Decoding"
        command = "lame --decode"
        print "\n Decode"

    print "Folder base " + folder_base

    relative_path = folder.replace(folder_base, "")
    print "Relative path " + relative_path

    new_path = folder_base + slash + container_dir + relative_path
    print "New path " + new_path

    if not encode:
        create_directory(new_path)
    else:
        create_directory(new_path.replace(slash + "decoded", ""))

    directories = []

    music_files = []

    for read_file in os.listdir(folder):

        name = read_file.split(".")

        print "Read file " + read_file

        if os.path.isdir(os.path.join(folder, read_file)):
            if read_file != "encoded" and read_file != "decoded" and read_file != "last":
                directories.append(read_file)

        # print "Test name [" + ";".join(name) + "]"

        if len(name) > 1:
            if not encode:
                if name[-1] == "mp3" or name[-1] == "MP3":
                    music_files.append(read_file)

            else:
                if name[-1] == "wav" or name[-1] == "WAV":
                    music_files.append(read_file)

        music_files.append("last")

    for music_file in music_files:

        print "Music file " + music_file
        if music_file != "last":
            decode_encode(music_file, folder, new_path, action_text, slash, command, encode)

    print "Starting for loop = [" + ";".join(directories) + "]"

    for directory in directories:

        print "Directory " + os.path.join(folder, directory)

        encode_music(os.path.join(folder, directory), folder_base, acceptable_formats, slash, encode)


def decode_encode(read_file, folder, new_path, action_text, slash, command, encode):

    print action_text + " file - " + read_file

    extension = read_file.split(".")[-1]

    file_name = read_file.replace(extension, "")

    in_file = folder + slash + read_file

    in_file = get_os_param("quote") + in_file.replace(" ", get_os_param("space_replace")) \
        + get_os_param("quote")

    if encode:
        new_path = new_path.replace(slash + "decoded", "")
        extension = "mp3"

    else:
        extension = "wav"
        file_name = file_name.replace(" ", "_").lower()

    out_file = new_path + slash + file_name + extension

    out_file = get_os_param("quote") + out_file.replace(" ", get_os_param("space_replace")) \
        + get_os_param("quote")

    cmd_full = command + " " + in_file + " " + out_file

    if encode:
        encode_commands.append(cmd_full)

    else:
        decode_commands.append(cmd_full)
    # execute_command(cmd_full, verbose)


def create_directory(path):
    print "Trying to create " + path
    if not os.path.exists(path):
        print "Creating directory " + path
        os.makedirs(path)


def execute_multi_thread_precess(commands):
    pool = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())

    for command in commands:
        pool.apply_async(execute_command, (command,))

    pool.close()
    pool.join()


if __name__ == "__main__":
    run(sys.argv[1:])
    print decode_commands[:]
    print len(decode_commands)
    execute_multi_thread_precess(decode_commands)
    print encode_commands[:]
    print len(encode_commands)
    execute_multi_thread_precess(encode_commands)